from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Schedule
from .forms import Schedule_Form


def homepage(request):
    return render(request, "story3.html")
def aboutme(request):
    return render(request, "story3-page2.html")
def register(request):
    return render(request, "register.html")

# for displaying the Schedule that have been added
def jadwal(request):
	response = {}
	lstJadwal = Schedule.objects.all()
	response = {
		"lstJadwal" : lstJadwal
	}

	return render(request, "jadwal.html", response)

# to display and get the information from the given Form
def tambahJadwal(request):
	form = Schedule_Form(request.POST or None)
	response = {}
	if(request.method == "POST"):
		if(form.is_valid()):
			nama_kegiatan = request.POST.get("nama_kegiatan")
			hari = request.POST.get("hari")
			date = request.POST.get("date")
			time = request.POST.get("time")
			tempat = request.POST.get("tempat")
			kategori = request.POST.get("kategori")

			Schedule.objects.create(
				nama_kegiatan = nama_kegiatan,
				hari = hari,
				date = date,
				time = time,
				tempat = tempat,
				kategori = kategori
			)

			return redirect('jadwal')
		else:
			return render(request, 'bikinJadwal.html', response)
	else:
		response['form'] = form
		return render(request, 'bikinJadwal.html', response)

def deleteJadwal(request):
	# delete all objects from Schedule database table

	Schedule.objects.all().delete()
	response = {}
	return render(request, 'jadwal.html', response)


	


