from django import forms
from .models import Schedule

class Schedule_Form(forms.Form):
	# hari, tanggal, jam, nama kegiatan, tempat, dan kategori
	nama_kegiatan = forms.CharField(label="Activity")
	hari = forms.CharField(label="Day")
	date = forms.DateField(widget=forms.DateInput(attrs={'type':'date'}), label="Date")
	time = forms.TimeField(widget=forms.TimeInput(attrs={'type':'time'}), label="Time")
	tempat = forms.CharField(label="Place")
	kategori = forms.CharField(label="Category")
