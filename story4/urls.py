from django.urls import path
from django.contrib import admin
from . import views 
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', views.homepage, name="story3.html"),
    path('', views.aboutme, name="story3-page2.html"),
    path('', views.register, name="register.html"),
    path('', views.tambahJadwal, name='tambahJadwal.html'),
    path('', views.jadwal, name='jadwal.html'),
    path('', views.deleteJadwal, name='jadwal.html')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)