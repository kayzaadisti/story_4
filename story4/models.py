from django.db import models

class Schedule(models.Model):
	nama_kegiatan = models.CharField(max_length=200)
	hari = models.CharField(max_length=20)
	date = models.DateField()
	time = models.TimeField()
	tempat = models.CharField(max_length=100)
	kategori = models.CharField(max_length=100)
