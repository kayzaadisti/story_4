"""myprofile URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.urls import re_path, include
from django.conf.urls import url, include
from story4.views import homepage as page1
from story4.views import aboutme as page2
from story4.views import register as page3
from story4.views import tambahJadwal as page4
from story4.views import jadwal as page5
from story4.views import deleteJadwal as hapusJadwal

urlpatterns = [
    path('admin/', admin.site.urls),
    path('story4/', include('story4.urls')),
    re_path(r'home', page1, name="home"),
    re_path(r'aboutme', page2, name="aboutme"),
    re_path(r'register', page3, name="register"),
    re_path(r'tambahJadwal', page4, name="tambahJadwal"),
    re_path(r'jadwal', page5, name="jadwal"),
    re_path(r'deleteJadwal', hapusJadwal, name="deleteJadwal")
]
